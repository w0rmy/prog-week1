﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convert_KM_to_Miles
{
    class Program
    {
        static void Main(string[] args)
        {
            var km = 0.0;
            var ml = 0.0;

            Console.WriteLine("How many Kilometers would you like to convert to Miles?");
            km = int.Parse(Console.ReadLine());
            ml = km * 0.621371;
            Console.WriteLine(ml);
        }
    }
}
