﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adding_Comments
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 1;
            var b = 2;

            var c = a + b;

            /// This seemed logical
            Console.WriteLine(a + b);
            
            /// And this seemed logical also, based off what we were taught earlier
            Console.WriteLine($"{a} + {b} = {c}");
        }
    }
}