﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Name
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Jeremy Brooking";
            var greeting = $"Hello {myName}";

            Console.WriteLine(greeting);
        }
    }
}
