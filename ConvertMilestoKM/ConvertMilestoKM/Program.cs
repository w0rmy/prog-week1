﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertMilestoKM
{
    class Program
    {
        static void Main(string[] args)
        {
            var km = 0.0;
            var ml = 0.0;

            Console.WriteLine("How many Miles would you like to convert to KM?");
            ml = int.Parse(Console.ReadLine());
            km = ml * 1.609344;
            Console.WriteLine(km);
        }
    }
}
