﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bedmas2
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 0;
            var b = 0;
            var c = 0;
            var d = 0;
            var e = 0;

            Console.WriteLine("Please enter 5 numbers");
            Console.WriteLine("First number");
            a = int.Parse(Console.ReadLine());

            Console.WriteLine("Second number");
            b = int.Parse(Console.ReadLine());

            Console.WriteLine("Third number");
            c = int.Parse(Console.ReadLine());

            Console.WriteLine("Forth number");
            d = int.Parse(Console.ReadLine());

            Console.WriteLine("Fifth number");
            e = int.Parse(Console.ReadLine());

            var bracket1 = a + b;
            var bracket2 = d - e;

            var result = (bracket1 * c / bracket2);

            Console.WriteLine($"(a + b) * c / (d - e) = {result}");
                    
        }
    }
}
